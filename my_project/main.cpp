#include "widget.h"
#include <QApplication>
#include "TcpServer.h"
#include "data_holder.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    TcpServer server;
    data_holder data( &server);


    Widget w(&data);
    w.show();

    return a.exec();
}
