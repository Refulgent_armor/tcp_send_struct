#ifndef TcpServer_H
#define TcpServer_H
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QByteArray>

class TcpServer : public QObject
{
    Q_OBJECT
public:
    explicit TcpServer(QObject *parent = 0);
public slots:
    void slotNewConnection();
    void slotServerRead();
    void slotClientDisconnected();
signals:
    void processingOfNewData(QByteArray data);
private:
    QTcpServer * mTcpServer;
    QTcpSocket * mTcpSocket;

};

#endif
