#ifndef DATA_HOLDER_H
#define DATA_HOLDER_H
#include <QObject>
#include <QByteArray>
#include "TcpServer.h"
#include <iostream>
#include <vector>

class data_holder: public QObject
{
   Q_OBJECT
public:
    data_holder(TcpServer * serv);
    struct strange_struct
    {
        char part;
        int number;
        strange_struct(char p,int n){part = p; number = n;};};
    std::vector<strange_struct> vector_of_data;


    void dataProcessing(QByteArray byteArray);
    void pushBackInMass(data_holder::strange_struct struct_of_data);
    std::vector<data_holder::strange_struct> getData(char type_data = 0);
    std::vector<data_holder::strange_struct> findData(char type_data);

signals:
   void Data_update();

private:
    data_holder::strange_struct deserialize(QByteArray byteArray);
    TcpServer * tcp_server;


};
#endif
