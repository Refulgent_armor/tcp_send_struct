#include "data_holder.h"
#include <QByteArray>
#include <QDataStream>


data_holder::data_holder(TcpServer * serv)
{
    tcp_server = serv;
    connect(tcp_server,  &TcpServer::processingOfNewData, this, &data_holder::dataProcessing);

}

data_holder::strange_struct data_holder::deserialize(QByteArray byteArray)
{

    QDataStream stream(byteArray);
    stream.setVersion(QDataStream::Qt_4_8);
    strange_struct returnedStruct(0,0);

    returnedStruct.number |= (unsigned char)byteArray[0];
    returnedStruct.number = returnedStruct.number<<8;
    returnedStruct.number |= (unsigned char)byteArray[1];
    returnedStruct.number = returnedStruct.number<<8;
    returnedStruct.number |= (unsigned char)byteArray[2];
    returnedStruct.number = returnedStruct.number<<8;
    returnedStruct.number |= (unsigned char)byteArray.at(3);


    returnedStruct.part = byteArray[4];
    returnedStruct.part = returnedStruct.part << 8;
    returnedStruct.part = byteArray[5];
    returnedStruct.part = returnedStruct.part << 8;
    returnedStruct.part = byteArray[6];
    returnedStruct.part = returnedStruct.part << 8;
    returnedStruct.part = byteArray[7];

    return(returnedStruct);

}

void data_holder::dataProcessing(QByteArray byteArray)
{
    if (byteArray.size() == 8)
    {
        strange_struct incoming_struct(0,0);

        incoming_struct = deserialize(byteArray);
        pushBackInMass(incoming_struct);
    }





}

void data_holder::pushBackInMass(strange_struct struct_of_data)
{
    vector_of_data.push_back(struct_of_data);
    emit Data_update();

}

std::vector<data_holder::strange_struct> data_holder::getData(char type_data)
{
    if (type_data)
        return(findData(type_data));
    else return(vector_of_data);
}

std::vector<data_holder::strange_struct> data_holder::findData(char type_data)
{
    std::vector<data_holder::strange_struct> vector_for_return;
    for(std::vector<data_holder::strange_struct>::const_iterator it = vector_of_data.begin();
         it!= vector_of_data.end();
         it++)
    {
        if (type_data == '0')
        {
            vector_for_return.push_back((*it));
        }
        else
        {
            if ((*it).part == type_data){
                vector_for_return.push_back((*it));
            };
        }
    }
    return(vector_for_return);
}
