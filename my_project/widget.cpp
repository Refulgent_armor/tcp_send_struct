#include <QFile>
#include "widget.h"
#include "ui_widget.h"
#include "data_holder.h"
Widget::Widget(data_holder * data,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    Cdata = data;
    connect(data, &data_holder::Data_update,this, &Widget::review_data);
    ui->setupUi(this);

}

Widget::~Widget()
{
    delete ui;
}



void Widget::on_button_first_clicked()
{
    current_show_mode = '1';
    show_data_from_dataclass(current_show_mode);

}
void Widget::on_button_second_clicked()
{
    current_show_mode = '2';
    show_data_from_dataclass(current_show_mode);
}
void Widget::on_button_all_clicked()
{
    current_show_mode = '0';
    show_data_from_dataclass(current_show_mode);
}


void Widget::review_data()
{
     show_data_from_dataclass(current_show_mode);
}

void Widget::show_data_from_dataclass(char part)
{
    ui->textBrowser->clear();
    QString text = data_to_string(part);
    ui->textBrowser->setText(text);

}

QString Widget::data_to_string(char part)
{
    QString text;
    std::vector<data_holder::strange_struct> vector_of_data = Cdata->getData(part);
    for(std::vector<data_holder::strange_struct>::const_iterator it = vector_of_data.begin();
         it!= vector_of_data.end();
         it++)
    {
        text.append("Prishli dannie '");
        text.append((int)((*it).number));
        text.append("' chast` ");
        text.append((*it).part);
        text.append("\r\n");
    }
    return(text);

}

void Widget::on_Button_save_clicked()
{
    QFile file("myfile.txt");
    file.open(QIODevice::WriteOnly);

    QString text = data_to_string(*"0");
    QByteArray s = text.toUtf8();
    file.write(s);

}
